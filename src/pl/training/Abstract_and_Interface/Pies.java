package pl.training.Abstract_and_Interface;

/**
 * Created by x on 2017-06-05.
 */
public class Pies extends Zwierze implements Szczekajace {

    @Override
    public void szczekaj() {
        System.out.println("pies szczeka po szczekajacych");

    }

    @Override
    public void oddychaj() {
        System.out.println("pies oddycha po zwierzecie");

    }
}
