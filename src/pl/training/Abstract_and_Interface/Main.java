package pl.training.Abstract_and_Interface;

import java.util.ArrayList;

/**
 * Created by x on 2017-06-05.
 */
public class Main {
    public static void main(String[] args) {

        ArrayList<Zwierze> zwierzeta = new ArrayList<>();
        zwierzeta.add(new Kot());
        zwierzeta.add(new Pies());

        for(Zwierze animals : zwierzeta){
            animals.oddychaj();
            System.out.println(animals);
        }
    }


}
