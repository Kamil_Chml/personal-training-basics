package pl.training.Abstract_and_Interface;

/**
 * Created by x on 2017-06-05.
 */
public class Kot extends Zwierze implements Miauczace {
    @Override
    public void miaucz(){
        System.out.println("kot miauczy po miauczacych");

    }
    @Override
    public void oddychaj(){
        System.out.println("kot oddycha po zwierzetach");

    }
}
