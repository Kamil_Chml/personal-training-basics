package pl.training;

/**
 * Created by x on 2017-05-31.
 */
public class Arrays_Basics {

    private static void main(String[] args) {

        char[] znaki;
        znaki = new char[14];
        znaki[1] = 'A';
        znaki[3] = 'V';

        char napis[] = {'w', 'f', 'f', 'd', 'f', 's', 'f', 's', 'd'};

        char tablicaznakow[];
        char[] tablicaznakowx;

        tablicaznakow = new char[2];

        char[] nowatablica = new char[5];

        char dzienDobry[] = new char[11];

        dzienDobry[0] = 'd';
        dzienDobry[1] = 'z';
        dzienDobry[2] = 'i';
        dzienDobry[3] = 'e';
        dzienDobry[4] = 'n';
        dzienDobry[5] = ' ';
        dzienDobry[6] = 'd';
        dzienDobry[7] = 'o';
        dzienDobry[8] = 'b';
        dzienDobry[9] = 'r';
        dzienDobry[10] = 'y';

        System.out.println();

        System.out.println(dzienDobry);

        char dzienDobryDwa[] = new char[11];

        dzienDobryDwa[0] = 'd';
        dzienDobryDwa[1] = 'z';
        dzienDobryDwa[2] = 'i';
        dzienDobryDwa[3] = 'e';
        dzienDobryDwa[4] = 'n';
        dzienDobryDwa[5] = ' ';
        dzienDobryDwa[6] = 'd';
        dzienDobryDwa[7] = 'O';
        dzienDobryDwa[8] = 'B';
        dzienDobryDwa[9] = 'r';
        dzienDobryDwa[10] = 'y';

        for (char x : dzienDobryDwa) {
            System.out.println(x);
        }
        System.out.println();

        System.out.println("drukuje w jednej lini");
        for (char y : dzienDobryDwa) {
            System.out.print(y);
            System.out.print(" ");
        }
        System.out.println();

        System.out.println("drukuje dużą czcionkę");
        System.out.println();
        for (char z : dzienDobryDwa) {

            System.out.print(Character.toUpperCase(z));
        }
        System.out.println();

        System.out.println("drukuje małą czcionkę");
        System.out.println();
        for (char t : dzienDobryDwa) {

            System.out.print(Character.toLowerCase(t));
        }
        System.out.println();
        System.out.println();

        char[] programowanie = {'p', 'r', 'o', 'g', 'r', 'a', 'm', 'o', 'w', 'a', 'n', 'i', 'e'};

        System.out.println("dane wejsciowe w tablicy:");
        System.out.println(programowanie);

        System.out.println("drukowanie pierwszej litery wielkiej");
        programowanie[0] = Character.toUpperCase(programowanie[0]);
        System.out.println(programowanie);
        System.out.println("druowanie w sorkdu litery duzej");
        programowanie[6] = Character.toUpperCase(programowanie[6]);
        System.out.println(programowanie);

        System.out.println("zamiana wszystkich liter na wielkie");

        for (int i = 0; i < programowanie.length; i++) {
            programowanie[i] = Character.toUpperCase(programowanie[i]);

        }
        System.out.println(programowanie);

        System.out.println();
        System.out.println("wyswietlamy znaki od konca do poczatku");

        for (int i = programowanie.length - 1; i >= 0; i--) {
            System.out.print(programowanie[i]);
        }
        for (int i = 0; i < programowanie.length; i++) {
            programowanie[i] = Character.toLowerCase(programowanie[i]);


        }
        System.out.println();
        System.out.println("przestawianie znakow pierwszy z ostatnim, drugi z przedostatnim itd");
        System.out.println(programowanie);

        for (int i = 0, j = programowanie.length - 1; i < j; i++, j--) {
            char tmp = programowanie[i];
            programowanie[i] = programowanie[j];
            programowanie[j] = tmp;
        }
        System.out.println(programowanie);

        System.out.println();
        char[] tablicazX = new char[10];
        System.out.println();
        System.out.println();
        System.out.println("Napisz program, który utworzy dziesięcioelementową tablicę znaków i wypełni ją cyframi " +
                "od 0 do 9, wykorzystaj  tabelę kodów ASCII i rzutowanie tak aby program wypisał cyfry ukłądu dziesiątkowego");

        char[] tablicaZnakow = new char[10];
        for (int i = 0; i < 10; i++) {
            tablicaZnakow[i] = (char) (i + 48);
        }
        System.out.println("cyfry układu dzieiatkowego: ");
        System.out.println(tablicaZnakow);


    }


}
