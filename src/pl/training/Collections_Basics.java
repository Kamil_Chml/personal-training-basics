package pl.training;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by x on 2017-06-01.
 */
public class Collections_Basics {

    private static Collection<String> kraje;

    public static void main(String[] args) {
        fillCountries();

        ArrayList<String> listaImion = new ArrayList<>(1);
        System.out.println(listaImion.size());

        listaImion.add("kamil");
        listaImion.add("aga");
        listaImion.add("fredek");
        System.out.println(listaImion.size());

        if (listaImion.contains("kamil")){
            System.out.println("tak zawiera");

        }else
            System.out.println("nie");

        System.out.println("przegladamy liste krajow:");
        System.out.println();

        for (String s : kraje)
            System.out.println(s);

        System.out.println("liczy ile jest krajow");


        System.out.println(kraje.size());

        System.out.println("ilosc krajow zaczynajacych sie na litere 'B' : ");

        System.out.println(iloscKrajowNaA(kraje));

        System.out.println();
        System.out.println("liczy kraje ktorych nazwa jest krotsza niz 6 liter");
        System.out.println();

        List<String> x = krajeKrotszeNiz6Liter(kraje);
        System.out.println(x.size());

        System.out.println();
        System.out.println("ilosc krajow, ktore nie zawieraja sekwencji 'ia' ");
        System.out.println(deleteOWO(kraje));





    }

    private static int iloscKrajowNaA(Collection<String> nazwaKolekcji){
        int count = 0;
        for (String s : nazwaKolekcji) {
            if (s.startsWith("b") || s.startsWith("B")){
                count ++;
            }
        }

        return count;
    }

    private static List<String> krajeKrotszeNiz6Liter (Collection<String > jakasKolekcja){
        Iterator<String> it = jakasKolekcja.iterator();
        ArrayList<String> zwracanaKolekcja = new ArrayList<>();
        while (it.hasNext()){
            String n = it.next();
            if (n.length()<6){
                zwracanaKolekcja.add(n);
            }
        }
        return zwracanaKolekcja;
    }
    private static int deleteOWO(Collection<String > jakasKolekcja){
        Iterator<String> it = jakasKolekcja.iterator();
        while (it.hasNext()){
            String x = it.next();
            if (x.endsWith("ia")){
                it.remove();
            }
        }
        return jakasKolekcja.size();
    }


    private static void fillCountries(){
        kraje = new ArrayList<>();
        kraje.add("Korea Południowa");
        kraje.add("Korea Północna");
        kraje.add("Kosowo");
        kraje.add("Kostaryka");
        kraje.add("Kuba");
        kraje.add("Kuwejt");
        kraje.add("Laos");
        kraje.add("Lesotho");
        kraje.add("Liban");
        kraje.add("Liberia");
        kraje.add("Libia");
        kraje.add("Liechtenstein");
        kraje.add("Litwa");
        kraje.add("Luksemburg");
        kraje.add("Łotwa");
        kraje.add("Macedonia");
        kraje.add("Madagaskar");
        kraje.add("Malawi");
        kraje.add("Malediwy");
        kraje.add("Malezja");
        kraje.add("Mali");
        kraje.add("Malta");
        kraje.add("Maroko");
        kraje.add("Bośnia I Hercegowina");
        kraje.add("Botswana");
        kraje.add("Brazylia");
        kraje.add("Brunei");
        kraje.add("Bułgaria");
        kraje.add("Burkina Faso");
        kraje.add("Burundi");
        kraje.add("Chile");
        kraje.add("Chińska Republika Ludowa");
        kraje.add("Chorwacja");
        kraje.add("Cypr");
        kraje.add("Cypr Północny");
        kraje.add("Czad");
        kraje.add("Czarnogóra");
        kraje.add("Dania");
        kraje.add("Demokratyczna Republika Konga");
        kraje.add("Dominika");
        kraje.add("Dominikana");
        kraje.add("Dżibuti");
        kraje.add("Egipt");
        kraje.add("Ekwador");
        kraje.add("Erytrea");
        kraje.add("Estonia");
        kraje.add("Etiopia");
        kraje.add("Fidżi");
        kraje.add("Filipiny");
        kraje.add("Finlandia");
        kraje.add("Francja");
        kraje.add("Gabon");
        kraje.add("Gambia");
        kraje.add("Ghana");
        kraje.add("Górski Karabach");
        kraje.add("Grecja");
        kraje.add("Grenada");
        kraje.add("Gruzja");
        kraje.add("Gujana");
        kraje.add("Gwatemala");
        kraje.add("Gwinea");
        kraje.add("Abchazja");
        kraje.add("Afganistan");
        kraje.add("Albania");
        kraje.add("Algieria");
        kraje.add("Andora");
        kraje.add("Angola");
        kraje.add("Antigua I Barbuda");
        kraje.add("Arabia Saudyjska");
        kraje.add("Argentyna");
        kraje.add("Armenia");
        kraje.add("Australia");
        kraje.add("Austria");
        kraje.add("Azerbejdżan");
        kraje.add("Bahamy");
        kraje.add("Bahrajn");
        kraje.add("Bangladesz");
        kraje.add("Barbados");
        kraje.add("Belgia");
        kraje.add("Belize");
        kraje.add("Benin");
        kraje.add("Bhutan");
        kraje.add("Białoruś");
        kraje.add("Birma");
        kraje.add("Boliwia");
        kraje.add("Gwinea Bissau");
        kraje.add("Gwinea Równikowa");
        kraje.add("Haiti");
        kraje.add("Hiszpania");
        kraje.add("Holandia");
        kraje.add("Somaliland");
        kraje.add("Sri Lanka");
        kraje.add("Stany Zjednoczone");
        kraje.add("Suazi");
        kraje.add("Sudan");
        kraje.add("Surinam");
        kraje.add("Syria");
        kraje.add("Szwajcaria");
        kraje.add("Szwecja");
        kraje.add("Tadżykistan");
        kraje.add("Tajlandia");
        kraje.add("Tajwan");
        kraje.add("Tanzania");
        kraje.add("Timor Wschodni");
        kraje.add("Togo");
        kraje.add("Tonga");
        kraje.add("Trynidad I Tobago");
        kraje.add("Tunezja");
        kraje.add("Turcja");
        kraje.add("Turkmenistan");
        kraje.add("Tuvalu");
        kraje.add("Uganda");
        kraje.add("Ukraina");
        kraje.add("Urugwaj");
        kraje.add("Honduras");
        kraje.add("Indie");
        kraje.add("Indonezja");
        kraje.add("Irak");
        kraje.add("Iran");
        kraje.add("Irlandia");
        kraje.add("Islandia");
        kraje.add("Izrael");
        kraje.add("Jamajka");
        kraje.add("Japonia");
        kraje.add("Jemen");
        kraje.add("Jordania");
        kraje.add("Kambodża");
        kraje.add("Kamerun");
        kraje.add("Kanada");
        kraje.add("Katar");
        kraje.add("Kazachstan");
        kraje.add("Kenia");
        kraje.add("Kirgistan");
        kraje.add("Kiribati");
        kraje.add("Kolumbia");
        kraje.add("Komory");
        kraje.add("Kongo");
        kraje.add("Mauretania");
        kraje.add("Mauritius");
        kraje.add("Meksyk");
        kraje.add("Mikronezja");
        kraje.add("Mołdawia");
        kraje.add("Monako");
        kraje.add("Mongolia");
        kraje.add("Mozambik");
        kraje.add("Naddniestrze");
        kraje.add("Namibia");
        kraje.add("Nauru");
        kraje.add("Nepal");
        kraje.add("Niemcy");
        kraje.add("Niger");
        kraje.add("Nigeria");
        kraje.add("Nikaragua");
        kraje.add("Norwegia");
        kraje.add("Nowa Zelandia");
        kraje.add("Oman");
        kraje.add("Osetia Południowa");
        kraje.add("Pakistan");
        kraje.add("Palau");
        kraje.add("Palestyna");
        kraje.add("Panama");
        kraje.add("Papua Nowa Gwinea");
        kraje.add("Paragwaj");
        kraje.add("Peru");
        kraje.add("Polska");
        kraje.add("Portugalia");
        kraje.add("Republika Czeska");
        kraje.add("Republika Południowej Afryki");
        kraje.add("Republika Środkowoafrykańska");
        kraje.add("Republika Zielonego Przylądka");
        kraje.add("Rosja");
        kraje.add("Wietnam");
        kraje.add("Włochy");
        kraje.add("Wybrzeże Kości Słoniowej");
        kraje.add("Wyspy Marshalla");
        kraje.add("Wyspy Salomona");
        kraje.add("Wyspy Świętego Tomasza I Książęca");
        kraje.add("Zambia");
        kraje.add("Zimbabwe");
        kraje.add("Zjednoczone Emiraty Arabskie");
        kraje.add("Rumunia");
        kraje.add("Rwanda");
        kraje.add("Sahara Zachodnia");
        kraje.add("Saint Kitts I Nevis");
        kraje.add("Saint Lucia");
        kraje.add("Saint Vincent I Grenadyny");
        kraje.add("Salwador");
        kraje.add("Samoa");
        kraje.add("San Marino");
        kraje.add("Senegal");
        kraje.add("Serbia");
        kraje.add("Seszele");
        kraje.add("Sierra Leone");
        kraje.add("Singapur");
        kraje.add("Słowacja");
        kraje.add("Słowenia");
        kraje.add("Somalia");
        kraje.add("Uzbekistan");
        kraje.add("Vanuatu");
        kraje.add("Watykan");
        kraje.add("Wenezuela");
        kraje.add("Węgry");
        kraje.add("Wielka Brytania");

    }
}
