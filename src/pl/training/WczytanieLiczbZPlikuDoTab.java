package pl.training;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by x on 2017-06-05.
 */
public class WczytanieLiczbZPlikuDoTab {
    public static void main(String[] args) throws IOException {
        String liczby = "F:\\CodeProjects\\myself training\\Training Basics\\cyferki.txt";
        int [] zwracanaTab = text2Tab(liczby);
        System.out.println(text2Tabv2(liczby));

        System.out.println(Arrays.toString(zwracanaTab));


    }

    public static String text2Tabv2(String fileIn) throws IOException {
        FileReader fileReader = new FileReader(fileIn);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        try {
            String liczbyZpliku = bufferedReader.readLine();
            do {
                System.out.println(liczbyZpliku);
                liczbyZpliku = bufferedReader.readLine();

            } while (liczbyZpliku != null);

            return liczbyZpliku;
        } finally {
            bufferedReader.close();
        }


    }

    public static int[] text2Tab(String liczby) {
        Scanner sc = null;
        int odczytanaLiczba;
        int[] tab = new int[0];

        try {
            sc = new Scanner(new File(liczby));
            odczytanaLiczba = sc.nextInt();
            tab = new int[odczytanaLiczba];
            for (int i = 0; i < tab.length; i++) {
                tab[i] = sc.nextInt();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(sc != null)
                sc.close();
        }
        return tab;
    }
}
