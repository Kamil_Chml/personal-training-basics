package pl.training;

/**
 * Created by x on 2017-05-31.
 */
public class Arguments {

    private static void main(String[] args) {

        System.out.println("Napisz program, który wyswietli na ekranie liczbe argumentów wywolania aplikacji\n" +
                "oraz podane argumenty. Kazdy argument powinien byc wyswietlony w odrebnym wierszu");
        System.out.println();
        // system operacyjny przekazuje podane argumenty do aplikacji w postaci tablicy
        System.out.println("liczba argumentów :" + args.length);
        for (String arguments : args)
            System.out.println(arguments);

        System.out.println();
        System.out.println("Napisz program, który jest uruchamiany z dwoma parametrami," +
                "imie i nazwisko, wyswietli na ekranie w kolejnych wierszach te dane wedlug " +
                "schematu:\n" +
                "Nazwisko: Kowalska\n" +
                "Imie: Maria\n" +
                "Nazwisko i imie: KOWALSKA Maria\n" +
                "Inicjaly: MK\n" +
                "Login: KOmar");



    }
}
