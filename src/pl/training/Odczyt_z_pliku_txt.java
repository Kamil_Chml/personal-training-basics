package pl.training;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by x on 2017-06-02.
 */
public class Odczyt_z_pliku_txt {
    public static void main(String[] args) throws IOException {
        File f = new File ("F:\\CodeProjects\\myself training\\Training Basics\\notatki.txt");

        if (f.isDirectory()){
            for (String s : f.list()){
                System.out.println(s);
            }
        }

        System.out.println("scieżka wskazanego pliku: " + f.getAbsolutePath());
        odczyt("F:\\CodeProjects\\myself training\\Training Basics\\notatki.txt");
    }
    public static void odczyt(String filepath) throws IOException {


        FileReader fileReader = new FileReader(filepath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        try {


            String tekstZPliku = bufferedReader.readLine();

            do {
                System.out.println(tekstZPliku);
                tekstZPliku = bufferedReader.readLine();
            }
            while (tekstZPliku != null);

        }
        finally {
            bufferedReader.close();
        }

    }
}
