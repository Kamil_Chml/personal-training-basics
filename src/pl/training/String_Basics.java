package pl.training;

/**
 * Created by x on 2017-05-31.
 */
public class String_Basics {
    private static void main(String[] args) {
        System.out.println("Zadania z programowania."); // drukuje
        System.out.println("Zadania z programowania.".charAt(0)); // wskazuje znak na indexie 0
        System.out.println("Zadania z programowania.".length()); // sprawdza dlugosc łańcucha
        System.out.println("Zadania z programowania.".charAt(23)); // wskazuje znak na indeksie 23
        System.out.println("Zadania z programowania.".toUpperCase()); // czcionka capslock
        System.out.println("Zadania z programowania.".toLowerCase()); // mała czcionka
        System.out.println("Zadania z programowania.".indexOf('z')); // wskazuje index litery z
        System.out.println("Zadania z programowania.".indexOf("prog")); // wskauzje index poczatkowych " prog"
        System.out.println("Zadania z programowania.".replace('a', ' ')); // zamienia wszystkie litery "a" na "spacje"
        System.out.println("Zadania z programowania.".
                replace("adania", "dania")); // zamiania wyrazania w argumentacvh
        System.out.println("Zadania z programowania.".
                replaceAll("ania", "anka")); // zamienia wyrazenia w argumentach
        System.out.println("Zadania z programowania.".
                replaceFirst("ania", "anka"));
        System.out.println("Zadania z programowania.".substring(10)); //
        System.out.println("Zadania z programowania.".substring(10, 17));
        System.out.println("Zadania z programowania.".
                concat("\b z podpowiedziami."));
        System.out.println("Zadania z programowania." + "\b" +
                " z odpowiedziami.");

        String wymysloneZdanie = new String("Moje cwiczenia z programowania!");

        System.out.println(wymysloneZdanie); // drukuje
        System.out.println(wymysloneZdanie.charAt(0)); // wskazuje znak na indexie 0
        System.out.println(wymysloneZdanie.length()); // sprawdza dlugosc lansucha
        System.out.println(wymysloneZdanie.charAt(23)); // wskazuje znak na indeksie 23
        System.out.println(wymysloneZdanie.toUpperCase()); // czcionka capslock
        System.out.println(wymysloneZdanie.toLowerCase()); // mala czcionka
        System.out.println(wymysloneZdanie.indexOf('r')); // wskazuje inex litery z
        System.out.println(wymysloneZdanie.indexOf("prog")); // wskauzje index poczatkowych " prog"
        System.out.println(wymysloneZdanie.replace('a', ' ')); // zamienia wszystkie litery a na spacje
        System.out.println(wymysloneZdanie.
                replace("enia", "onka")); // zamiania wyrazania w argumentacvh
        System.out.println(wymysloneZdanie.
                replaceAll("ania", "anka")); // zamienia wyrazenia w argumentach
        System.out.println(wymysloneZdanie.
                replaceFirst("ania", "anka"));
        System.out.println(wymysloneZdanie.substring(10)); //
        System.out.println(wymysloneZdanie.substring(10, 17));
        System.out.println(wymysloneZdanie.
                concat("\b z podpowiedziami."));
        System.out.println(wymysloneZdanie + "\b" +
                " z odpowiedziami.");

        for (char zy : wymysloneZdanie.toCharArray())
            System.out.println(zy);


        for (char y : wymysloneZdanie.toCharArray())
            System.out.print(y + " ");

        System.out.println("-----------------------------------------");
        System.out.println();
        System.out.println();

        String lancuch = new String("Siema Sciema");
        for (char x : lancuch.toCharArray())
            System.out.println(x);
        System.out.println();
        for (char x : lancuch.toCharArray())
            System.out.print(x + " ");
        System.out.println();
        for (char x : lancuch.toCharArray())
            System.out.print(Character.toUpperCase(x));
        System.out.println();
        System.out.println(lancuch.toUpperCase());
        for (char x : lancuch.toCharArray())
            System.out.print(Character.toLowerCase(x));
        System.out.println();
        System.out.println(lancuch.toLowerCase());

        System.out.println("====================");

        String wyraz = new String("programista");

        System.out.println(Character.toUpperCase(wyraz.charAt(0)) + wyraz.substring(1));
        System.out.println(wyraz.replaceFirst("pr", "Pr"));


        System.out.println(wyraz.toUpperCase());

        char[] znakiwyrazu = wyraz.toCharArray();
        znakiwyrazu[0] = Character.toUpperCase(znakiwyrazu[0]);
        wyraz = String.valueOf(znakiwyrazu);
        System.out.println(wyraz);

        String tentego = new String("nie wiem co wymyslec");

        char[] tentegoX = tentego.toCharArray();
        for (int i = tentegoX.length - 1; i >= 0; i--) {
            System.out.print(tentegoX[i]);
        }
        tentego = new String(tentegoX);
        System.out.println();
        System.out.println(tentego + " z tab do stringu");


        for (int i = tentego.length() - 1; i >= 0; i--) {
            System.out.print(tentego.charAt(i));

        }


        System.out.println();
        String napisek = new String("Bogusia BlaBla");

        char[] tabelka = napisek.toCharArray();
        for (int i = 0, j = tabelka.length - 1; i < j; i++, j--) {
            char tmp = tabelka[i];
            tabelka[i] = tabelka[j];
            tabelka[j] = tmp;

        }
        napisek = new String(tabelka); // wrzucenie tablicy znakow w String!
        System.out.println(napisek);
        System.out.println();
        System.out.println();
        System.out.println("Napisz program, który utworzy łańcuch znaków wypełniony cyframi od 0 do 9");
        System.out.println();
        System.out.println();

        String cyfry = new String("0123456789");
        System.out.println(cyfry);
        System.out.println();
        System.out.println();
        System.out.println("Napisz program, który utworzy łańcuch znaków wypełniony cyframi od 0 do 9" +
        "Spróbuj jednak zbudować ten łańcuch z poszczególnych znaków poprzez układ ASCII");

        String lancuchCyferek = "";
        for (int i = 0; i <10 ; i++) {
            lancuchCyferek += (char)(i+48);


        }
        System.out.println(lancuchCyferek);

        System.out.println("alternatywne rozwiązanie:");

        char [] cyferki = new char [10];
        for (int i = 0; i <10 ; i++) {
            cyferki [i] = (char)(i+48);
        }
        String lancuchZCyferek = new String(cyferki);
        System.out.println(cyferki);
        System.out.println();
        System.out.println();
        System.out.println("Napisz program, który utworzy łańcuch znaków wypełniony cyframi układu szesnastkowego");
        System.out.println();
        String szesnastki = "";

        for (int i = 0; i <16 ; i++) {
            szesnastki += Character.forDigit(i,16);
            szesnastki += ",";

        }
        System.out.println();
        System.out.println("cyfry od 1 - 16 w układzie szesnastkowym: ");
        szesnastki = szesnastki.toUpperCase();
        System.out.println(szesnastki);




































    }
}
