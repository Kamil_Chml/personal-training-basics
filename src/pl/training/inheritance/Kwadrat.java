package pl.training.inheritance;

/**
 * Created by x on 2017-06-05.
 */
public class Kwadrat extends Figury {

    public Kwadrat ( float bokA){
        this.bokA = bokA;
    }

    @Override
    public float pole () {
        return this.bokA*this.bokA;
    }



}
