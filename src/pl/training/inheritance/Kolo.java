package pl.training.inheritance;

/**
 * Created by x on 2017-06-05.
 */
public class Kolo extends Figury {
    final static float PI = 3.1415f;
    public Kolo (float promien){
        this.bokA = promien;
    }

    @Override
    public float pole () {
        return this.bokA*this.bokA*PI;
    }
}
