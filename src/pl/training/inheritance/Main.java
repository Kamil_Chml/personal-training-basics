package pl.training.inheritance;

import java.util.ArrayList;

/**
 * Created by x on 2017-06-05.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Figury> figury = new ArrayList<>();
        figury.add(new Kwadrat(5));
        figury.add(new Prostokat(5,3));
        figury.add(new Kolo(5));

        for (Figury figury1 : figury){
            try {
                System.out.println(figury1.getClass());
                System.out.println(figury1.pole());

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
