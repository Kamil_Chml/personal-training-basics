package pl.training.inheritance;

/**
 * Created by x on 2017-06-05.
 */
public class Prostokat extends Figury {

    public Prostokat (float bokA, float bokB ) {
        this.bokA = bokA;
        this.bokB = bokB;
    }
    @Override
    public float pole () {
        return this.bokA*this.bokB;
    }
}
